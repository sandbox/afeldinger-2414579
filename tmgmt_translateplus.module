<?php

define('TMGMT_TRANSLATEPLUS_WORDS_PER_REQUEST', 5000);

define('TMGMT_TRANSLATEPLUS_API_CACHE_DISABLED', FALSE);
define('TMGMT_TRANSLATEPLUS_API_CACHE_EXPIRE', 60 * 60);

define('TMGMT_TRANSLATEPLUS_STATUS_NOT_STARTED', 0);
define('TMGMT_TRANSLATEPLUS_STATUS_IN_PROGRESS', 1);
define('TMGMT_TRANSLATEPLUS_STATUS_COMPLETED', 2);
define('TMGMT_TRANSLATEPLUS_STATUS_UPDATED', 3);

use Drupal\tmgmt\JobInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt\Entity\RemoteMapping;

/**
 * Fetch translators of type "translateplus".
 */
function tmgmt_translateplus_get_translators($translator_name = NULL) {
  $translators = $translators = Translator::loadMultiple();
  foreach ($translators as $name => $translator) {
    if ($translator->getPlugin() != 'translateplus') {
      unset($translators[$name]);
    }
  }

  if (!empty($translator_name)) {
    return (array_key_exists($translator_name, $translators)) ? $translators[$translator_name] : NULL;
  }
  else {
    return (!empty($translators)) ? $translators : NULL;
  }
}

/**
 * Fetch translation job IDs by reference.
 */
function tmgmt_translateplus_fetch_tjids_by_reference($reference = NULL) {
  if (!$reference) {
    return array();
  }

  if (!is_array($reference)) {
    $reference = array($reference);
  }

/*
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'tmgmt_remote', '=')
    ->propertyCondition('remote_identifier_1', $reference, 'in');
  $result = $query->execute();

  $tjids = array();

  if (isset($result['tmgmt_remote'])) {
    $trids = array();
    foreach ($result['tmgmt_remote'] as $item) {
      $trids[] = $item->trid;
    }
    $remote_items = entity_load('tmgmt_remote', $trids);
    foreach ($remote_items as $item) {
      $tjids[] = $item->tjid;
    }
    $tjids = array_unique($tjids);
  }
*/

  
  $query = \Drupal::entityQuery('tmgmt_remote')
    ->accessCheck(FALSE)
    ->condition('remote_identifier_1', $reference, 'in');
  $trids = $query->execute();

  $tjids = array();

  $remote_items = RemoteMapping::loadMultiple($trids);

  foreach ($remote_items as $item) {
    $tjids[] = $item->getJobId();
  }
  $tjids = array_unique($tjids);

  return $tjids;
}

/**
 * Fetch translation job IDs by reference.
 */
function tmgmt_translateplus_fetch_tjids_by_hash_key($key = NULL) {
  if (!$key) {
    return array();
  }
  
  $query = \Drupal::entityQuery('tmgmt_remote')
    ->accessCheck(FALSE)
    ->condition('remote_identifier_2', $key);
  $trids = $query->execute();

  $tjids = array();
  $remote_items = RemoteMapping::loadMultiple($trids);
  foreach ($remote_items as $item) {
    $tjids[] = $item->getJobId();
  }
  $tjids = array_unique($tjids);

  return $tjids;
}

/**
 * Implements hook_cron().
 */
function tmgmt_translateplus_cron() {
  // Check for any translation job items available for collection.
  tmgmt_translateplus_update_collectable_items(TRUE);
}

/**
 * Update collectable items from translate plus.
 */
function tmgmt_translateplus_update_collectable_items($cron_run = FALSE) {

  /** @var QueueFactory $queue_factory */
  $queue_factory = \Drupal::service('queue');

  /** @var QueueInterface $queue */
  // Fetch queue.
  $queue = $queue_factory->get('tmgmt_translateplus_collect');

  // Only fetch new items when queue is empty. Else return.
  if ($queue->numberOfItems()) {

    \Drupal::logger('tmgmt_translateplus')->info("Queue has @count items. Process those first.", array(
      '@count' => $queue->numberOfItems(),
    ));

    return;
  }

  // Fetch relevant translators
  $translators = $translators = Translator::loadMultiple();

  // Check all translate plus translators.
  foreach ($translators as $translator) {

    // If cron file collection is not enabled for this translator, skip to next.
    if ($cron_run === TRUE && !in_array($translator->getSetting('collection_method'), array('default', 'cron'))) {
      continue;
    }

    $plugin = $translator->getPlugin();

    // Merge all available completed and updated items for translator.
    $items = array();

    // Check "ready for collection" status codes.
    $check_status = array(TMGMT_TRANSLATEPLUS_STATUS_COMPLETED, TMGMT_TRANSLATEPLUS_STATUS_UPDATED);
    foreach ($check_status as $status_code) {
      // Retrieve items.
      $status_items = (array) $plugin->getJobItemsWithStatus($translator, $status_code);

      // Catch single-object returns and normalize into array structure.
      if (isset($status_items['TPWSJobItem']) && is_object($status_items['TPWSJobItem'])) {
        $status_items['TPWSJobItem'] = array($status_items['TPWSJobItem']);
      }
      $items += $status_items;
    }

    if (isset($items['TPWSJobItem'])) {
      // Run through all items.
      $q_added = [];
      $q_skipped = [];
      foreach ($items['TPWSJobItem'] as $item) {
        // Fetch tjid.
        $tjids = tmgmt_translateplus_fetch_tjids_by_reference($item->ParentJobOrderID);
        $tjid = array_shift($tjids);

        // Build data array.
        $data = array(
          'tmgmt_tjid' => $tjid,
          'translator' => $translator->label(),
          'item' => (array) $item,
        );

        // If item has valid ID, add to processing queue.
        if ($tjid) {
          $q_added[] = $data;
          $queue->createItem($data);
        }
        // If not, skip item
        else {
          $q_skipped[] = $data;
        }
      }

      // Debug
      if ($translator->getSetting('enable_debugging')) {
        \Drupal::logger('tmgmt_translateplus')->info("Added @added items to queue. Omitted @skipped items. <pre>@data</pre>", array(
          '@added' => count($q_added),
          '@skipped' => count($q_skipped),
          '@data' => var_export(array(
            'added' => empty($q_added)? null : $q_added,
            'omitted' => empty($q_skipped)? null : $q_skipped,
          ), true),
        ));
      }

    }
  }
}

/**
 * Queue processing function.
 */
function tmgmt_translateplus_process_queued_item($data) {


  if (!empty($data['tmgmt_tjid']) && !empty($data['item'])) {
    //$job = tmgmt_job_load($data['tmgmt_tjid']);
    $job = Job::load($data['tmgmt_tjid']);
    if ($job) {
      $translator = $job->getTranslator();
      
      // Debug
      if ($translator->getSetting('enable_debugging')) {
        \Drupal::logger('tmgmt_translateplus')->info("Processed queue item with data <pre>@data</pre>", array(
          '@data' => var_export($data, true),
        ));
      }

      return tmgmt_translateplus_retrieve_translation($job, $data['item']['ID']);
    }
  }


}

/**
 * Trigger translation file collection and import items into job.
 */
function tmgmt_translateplus_retrieve_translation(JobInterface $job, $tplus_jiid) {
  //$plugin = $job->getTranslatorController();
  $translator = $job->getTranslator();
  $plugin = $translator->getPlugin();
  $success = $plugin->retrieveTranslation($job, $tplus_jiid);
  return $success;
}


/**
 * Use batch_api to process translation request submissions.
 */
function tmgmt_translateplus_batch_submit_translations(JobInterface $job, $service, $data_parts, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($data_parts);
    $context['sandbox']['parts'] = $data_parts;
    $context['results'] = array(
      'job' => $job,
      'total' => count($data_parts),
      'success' => array(),
      'error' => array(),
    );
  }

  $item = array_shift($context['sandbox']['parts']);
  $translator = $job->getTranslator();
  $plugin = $translator->getPlugin();

  // Request translation of part.
  $success = $plugin->requestTranslationProcessPart($job, $service, $item);

  if ($success !== FALSE) {
    $context['results']['success'][$item['part_key']] = $item;
  }
  else {
    $context['results']['error'][$item['part_key']] = $item;
  }

  $context['sandbox']['progress']++;
  $context['message'] = t('Submitting %part_key (@word_count words)', array('%part_key' => $item['part_key'], '@word_count' => $item['part']['word_count']));

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Complete batch submission of translations.
 */
function tmgmt_translateplus_batch_submit_translations_complete($success, $results, $operations) {

  $job = $results['job'];
  $translator = $job->getTranslator();
  $plugin = $translator->getPlugin();

  if ($success) {
    if (!empty($results['success'])) {
      $message = t('@count job parts successfully submitted to translate plus.', array('@count' => count($results['success'])));
      //$message .= theme('item_list', array('items' => array_keys($results['success'])));
      \Drupal::messenger()->addMessage($message);
    }
    if (!empty($results['error'])) {
      $message = t('@count job parts failed submission to translate plus.', array('@count' => count($results['error'])));
      //$message .= theme('item_list', array('items' => array_keys($results['error'])));
      \Drupal::messenger()->addMessage($message, MessengerInterface::TYPE_ERROR);
    }
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t(
      'An error occurred while processing %error_operation with arguments: @arguments',
      array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      )
    );
    \Drupal::messenger()->addMessage($message, MessengerInterface::TYPE_ERROR);
  }

  $plugin->requestTranslationProcessComplete($job, $results);
}


/**
 * Override API account GUID if set in settings.php $conf array.
 */
function tmgmt_translateplus_api_guid(\Drupal\tmgmt\Entity\Translator $translator) {
  static $api_guid;
  if (isset($api_guid)) {
    return $api_guid;
  }

  $api_guid = FALSE;

  global $conf;
  // If key exists in $conf array, use that.
  if (!empty($conf['translateplus_api_account_guid'])) {
    $api_guid = $conf['translateplus_api_account_guid'];
  }
  // next, check translator setting.
  else {
    $api_guid = $translator->getSetting('api_account_guid');
  }

  return $api_guid;
}


/**
 * Create secret hash for translate plus reference.
 */
function tmgmt_translateplus_hash($id, $key = '') {
  return md5(\Drupal\Core\Site\Settings::getHashSalt() . $id . $key);
}
