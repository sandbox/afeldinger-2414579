<?php

namespace Drupal\tmgmt_translateplus\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides cron worker functionality
 */

/**
 * A Cron worker that retrieves TranslatePlus jobs on CRON run.
 *
 * @QueueWorker(
 *   id = "tmgmt_translateplus_collect",
 *   title = @Translation("TranslatePlus collection manager"),
 *   cron = {"time" = 20}
 * )
 */
class TranslatePlusQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  // Here we don't use the Dependency Injection, 
  // but the create method and __construct method are necessary to implement

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
  	return tmgmt_translateplus_process_queued_item($data);
  }
}